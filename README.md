# Ubuntu-WP-PHPRedis
This project provides a Docker image that is based on Ubuntu. This makes it possible to provide the Apache web server, PHP and Wordpress from the official package sources.
If you use Redis as a fast object cache, this image includes PHPRedis, which makes it very easy to connect to Redis from within Wordpress.

The current release of the docker-image you can find here:

`https://hub.docker.com/repository/docker/laszlo/ubuntu-wp-phpredis/general`

You can download it from my private registry (free of download). It is exactly the image of what the dockerfile creates.

If you see the error (unsecure content over secure connection (or else)), you should put my registry
registry.gretzki.ddns.net to your docker daemon json.
Or on docker desktop in configuration or else.



## Getting started

The result of this image is an alternative wordpress installation, based on
- Ubuntu (currently 22.04)
- Wordpress (current version 6.1.1, dependently on download)
- Apache Webserver 2
- PHP 8.1
- phpredis (as part of apt the current version on buildtime)

## Usage

There are a few environment variables that can be passed in from into the Docker container.
An runnable example you can find here.
The docker-compose starts MySQL, Redis and Wordpress. The following connection-settings works with the configuration.

```
- WORDPRESS_DB_HOST: db:3306
- WORDPRESS_DB_USER: wordpress
- WORDPRESS_DB_PASSWORD: wordpress
- WORDPRESS_DB_NAME: wordpress
- WORDPRESS_AUTH_KEY: "put your unique phrase here"
- WORDPRESS_SECURE_AUTH_KEY: "put your unique phrase here"
- WORDPRESS_LOGGED_IN_KEY: "put your unique phrase here"
- WORDPRESS_NONCE_KEY: "put your unique phrase here"
- WORDPRESS_AUTH_SALT: "put your unique phrase here"      
- WORDPRESS_SECURE_AUTH_SALT: "put your unique phrase here"
- WORDPRESS_LOGGED_IN_SALT: "put your unique phrase here"
- WORDPRESS_NONCE_SALT: "put your unique phrase here"
- WORDPRESS_CONFIG_EXTRA: |
    define('FORCE_SSL_ADMIN', true);
    define('FORCE_SSL_LOGIN', true);
    define('WP_HOME','http://192.168.0.102/');
    define('WP_SITEURL','http://192.168.0.102/');
    define('WP_REDIS_HOST', 'wordpress_redis_1');
    define('WP_REDIS_PORT', 6379);
    define('WP_REDIS_TIMEOUT', 1);
    define('WP_REDIS_READ_TIMEOUT', 1);
    define('WP_REDIS_DATABASE', 0);
    define('WP_REDIS_CLIENT', 'phpredis');
    define('WP_REDIS_DISABLED', false);
    define('WP_MEMORY_LIMIT', '1024M');
```

The Security keys and Salts can be
You can create these things with Linux, for example.
A working alternative with ubuntu (and derivates) is

`cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-48} | head -n 1`

The command produces something like this:
`JZKlfcTLknkljAyMVMWxwjmcuQQgayEWSZRDIQTVesfsjYYH`

In an earlier version i put the command for code-generation inside of the dockerfile, but with >1 started containers the same keys would be used, which makes the whole thing insecure again.

The block WORDPRESS_CONFIG_EXTRA is copied from the original docker wp-config.template. Here you can define some useful things, such as Redis configuration, memory or upload settings or whatever.

Attention!
If you plan to use this image in production, you need to consider the following.
When starting a container from this image, all contents are inside the container. If the container is restarted, for example with `docker rm ....`, all contents are lost.
The following procedure is suitable for this:
1. start it for the first time using docker-compose
2. copy the content from `/var/www/html` to the host with `docker cp ...` to your host.
3. adjust the docker-compose.yml (volume from host directory to `/var/www/html`) mount.
4. change the owner of the directory on the host (`chown -R www-data:www-data directory`).
5. Change permissions for files and directories for the copied host directory (`chmod -R 755 directory`).

The next start with docker-compose gets all the wordpress stuff from your mounted volume.
Theoretically you don´t need all the environment parameters. You can now edit the wp-config.php file and set the needed parameters directly.
