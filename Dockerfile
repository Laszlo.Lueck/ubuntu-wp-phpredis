FROM --platform=linux/amd64 ubuntu:22.04
RUN uname -a
ENV TZ=Europe/Berlin
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
&& echo $TZ > /etc/timezone \
&& apt-get clean \
&& apt-get update \
&& apt-get -y upgrade \
&& dpkg --configure -a \
&& apt-get install -y ca-certificates apt-utils wget php apache2 php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-redis \
&& update-ca-certificates \
&& rm -rf /var/lib/apt/lists/*
COPY ./files/apache2.conf /etc/apache2/apache2.conf
RUN a2enmod rewrite \
&& apache2ctl configtest \
&& service apache2 restart \
&& ln -sf /dev/stderr /var/log/apache2/error.log \
&& ln -sf /dev/stdout /var/log/apache2/access.log \
&& rm /var/www/html/*.* \
&& mkdir ./.tmp \
&& cd .tmp \
&& wget -q -O ./wordpress.tar.gz https://wordpress.org/wordpress-6.2.tar.gz \
&& tar xzf ./wordpress.tar.gz \
&& cp -a wordpress/. /var/www/html \
&& cd .. \
&& rm -rf ./.tmp \
&& mkdir /var/www/html/wp-content/uploads 
COPY ./files/.htaccess /var/www/html/
COPY ./files/wp-config.php /var/www/html/
RUN chown -R www-data:www-data /var/www/html \
&& chmod -R 755 /var/www/html \
&& apt-get clean
EXPOSE 80
EXPOSE 443
CMD apachectl -D FOREGROUND
